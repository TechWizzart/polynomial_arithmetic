using System;

namespace polynomial_arithmetic {
    public class NoFieldException : Exception {
        public NoFieldException() {
        }

        public NoFieldException(string poly) : base(String.Format("Polynomial {0} is not a member of a field", poly)) {
        }

        public NoFieldException(string poly, Exception inner) : base(String.Format("Polynomial {0} is not a member of a field", poly), inner) {
        }
    }

    public class FieldException : Exception {
        public FieldException() {
        }

        public FieldException(string poly0, string poly1) : base(String.Format("Polynomial {0} and {1} are not elements of the same field", poly0, poly1)) {
        }

        public FieldException(string poly0, string poly1, Exception inner) : base(String.Format("Polynomial {0} and {1} are not elements of the same field", poly0, poly1), inner) {
        }
    }

    public class NotPrimeException : Exception {
        public NotPrimeException(){
        }

        public NotPrimeException(string poly, string mod) : base(String.Format("Polynomial {0} with modulo {1} does not have a prime modulus", poly, mod)){
        }

        public NotPrimeException(string poly, string mod, Exception inner) : base(String.Format("Polynomial {0} with modulo {1} does not have a prime modulus", poly, mod), inner){
        }
    }

    public class ParseException : Exception {
        public ParseException() : base("Error occurred while parsing the input") { }

        public ParseException(string err) : base(err) { }
    }
    public class NoInverseException : Exception {
        public NoInverseException(){
        }

        public NoInverseException(string poly) : base(String.Format("Element {0} is not invertible", poly)){
        }

        public NoInverseException(string poly, Exception inner) : base(String.Format("Element {0} is not invertible", poly), inner){
        }
    }

    public class InvalidFieldException : Exception {
        public InvalidFieldException(){
        }
        public InvalidFieldException(string poly) : base(String.Format("Modulos polynomial {0} cannot be restrained by being an element of another field", poly)){
        }

        public InvalidFieldException(string poly, Exception inner) : base(String.Format("Modulos polynomial {0} cannot be restrained by being an element of another field", poly), inner){
        }
    }

    public class ReducibleException : Exception {
        public ReducibleException(){
        }

        public ReducibleException(string poly) : base(String.Format("Polynomial {0} is not irreducible", poly)){
        }

        public ReducibleException(string poly, Exception inner) : base(String.Format("Polynomial {0} is not irreducible", poly), inner){
        }
    }
}