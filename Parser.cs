using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace polynomial_arithmetic {
    static class Parser {
        // parses a file
        // can throw a ParseException
        public static List<Operation> parse(string filename) {
            try {
                // read file
                List<string> lines = readFile(filename);
                // convert to tokens
                List<Token> tokens = tokenize(lines);
                // convert tokens into operations
                return parse(tokens);
            } catch (Exception ex) {
                throw new ParseException(String.Format("Exception occurred while parsing: {0}", ex.ToString()));
            }
        }

        private static List<string> readFile(string filename) {
            // will contain the content of the file, as a list of strings (split on linebreaks)
            List<string> contents = new List<string>();

            // create filestream
            FileStream fs = new FileStream(filename, FileMode.Open);

            // read file line by line
            using (StreamReader reader = new StreamReader(fs)) {
                string line;

                // keep reading until there is nothing to read
                while ((line = reader.ReadLine()) != null) {
                    contents.Add(line);
                }
            }

            return contents;
        }

        private static List<Token> tokenize(List<string> lines) {
            List<Token> tokens = new List<Token>();

            for (int i = 0; i < lines.Count; i++) {
                // for conciseness
                string s = lines[i];
                // we remove all tabs and spaces, because they don't mean anything
                s = s.Replace(" ", "");
                s = s.Replace("\t", "");

                // if the string contains a #, it means that the rest of the line is a comment and we can remove it
                int loc = s.IndexOf('#');
                // if it contains a #, remove it and everything after it
                if (loc != -1) {
                    s = s.Remove(loc);
                }

                // if the line is empty, we need to skip it
                if (s.Length == 0) {
                    continue;
                }

                for (int j = 0; j < Token.SYMBOLS.Length; j++) {
                    string symbol = Token.SYMBOLS[j];

                    if (s.StartsWith(symbol)) {
                        s = s.Substring(symbol.Length);

                        Token t = new Token(j, s);
                        tokens.Add(t);
                    }
                }
            }

            // remove the tokens that can be ignored if the file is input
            tokens = tokens.Where(x => !(new int[] {9, 10, 12, 13, 14, 33}).Contains(x.token)).ToList();

            // add EOF token (used by parser)
            tokens.Add(new Token((int) Token.TOKENS.EOF, "EOF"));

            return tokens;
        }

        private static List<Operation> parse(List<Token> tokens) {
            /**
                We expect every input to follow the following format:

                1:   mod
                2:   mod-poly    (optional)
                3:   operation
                4:   input       (optional)   can be either [f] or [a] or [deg]
                5:   input       (optional)   can be either [g] or [b]
                6:   input       (optional)   can only be [h]

                Allowed combinations of the last three optional inputs are:
                [deg]
                [f]
                [f] [g]
                [f] [g] [h]
                [a]
                [a] [b]
             */

            // will contain the operations that are generated from the list of tokens
            List<Operation> operations = new List<Operation>();

            // a state variable to keep track of what we need to parse/do
            int state = 0;

            // temporary variables to store the information we have parsed so far
            int mod, op_code, deg;
            mod = op_code = deg = -1;
            Poly mod_poly, a, b, f, g, h;
            mod_poly = a = b = f = g = h = null;

            // we loop over every token, act based on the value of that token and the state
            for (int i = 0; i < tokens.Count; i++) {
                Token t = tokens[i];
                // Console.WriteLine(String.Format("{0} {1}", Token.SYMBOLS[t.token], t.argument));
                // Console.WriteLine("State: " + state);
                switch (state) {
                    case 0:
                        if (t.token != (int) Token.TOKENS.MOD) {
                            throw new ParseException(String.Format("Expected {0}, but got {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], Token.SYMBOLS[t.token]));
                        }

                        mod = Int32.Parse(t.argument);

                        state++;
                        break;
                    case 1:
                        if (t.token != (int) Token.TOKENS.MOD_POLY) {
                            // since this is optional we will not throw an error but continue to the next loop without consuming a token
                            state++;
                            i--;
                            break;
                        }

                        mod_poly = new Poly(parseCoefficients(t.argument), mod);

                        state++;
                        break;
                    case 2:
                        if (! Token.isOperation(t)) {
                            throw new ParseException(String.Format("Expected an operation but got {0}", Token.SYMBOLS[t.token]));
                        }

                        op_code = Token.getOperationCode(t);

                        state++;
                        break;
                    case 3:
                        if (! Token.isInput(t)) {
                            // if token is not an input token, we need to skip to the next state without consuming a token
                            state++;
                            i--;
                            break;;
                        }

                        switch (t.token) {
                            case (int) Token.TOKENS.DEG:
                                deg = Int32.Parse(t.argument);
                                break;
                            case (int) Token.TOKENS.A:
                                if (mod_poly == null) {
                                    throw new ParseException(String.Format("Unexpected {0}. Please specify {1} first", Token.SYMBOLS[(int) Token.TOKENS.A], Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY]));
                                }
                                a = new Poly(parseCoefficients(t.argument), mod, mod_poly);
                                break;
                            case (int) Token.TOKENS.F:
                                f = new Poly(parseCoefficients(t.argument), mod);
                                break;
                            default:
                                throw new ParseException(String.Format("Expected {0}, {1} or {2} but got {3}", Token.SYMBOLS[(int)Token.TOKENS.DEG], Token.SYMBOLS[(int)Token.TOKENS.A], Token.SYMBOLS[(int)Token.TOKENS.F], Token.SYMBOLS[t.token]));
                        }

                        state++;
                        break;
                    case 4:
                        if (! Token.isInput(t)) {
                            // if token is not an input token, we need to skip to the next state without consuming a token
                            state++;
                            i--;
                            break;;
                        }

                        switch (t.token) {
                            case (int) Token.TOKENS.B:
                                // check if a is set, throw error if not
                                if (a is null) {
                                    throw new ParseException(String.Format("Unexpected {0}", Token.SYMBOLS[(int) Token.TOKENS.B]));
                                }

                                b = new Poly(parseCoefficients(t.argument), mod, mod_poly);
                                break;
                            case (int) Token.TOKENS.G:
                                // check if f is set, throw error if not
                                if (f is null) {
                                    throw new ParseException(String.Format("Unexpected {0}", Token.SYMBOLS[(int) Token.TOKENS.G]));
                                }

                                g = new Poly(parseCoefficients(t.argument), mod);
                                break;
                            default:
                                throw new ParseException(String.Format("Unexpected {0}", Token.SYMBOLS[t.token]));
                        }

                        state++;
                        break;
                    case 5:
                        if (! Token.isInput(t)) {
                            // if token is not an input token, we need to skip to the next state without consuming a token
                            state++;
                            i--;
                            break;
                        }

                        switch (t.token) {
                            case (int) Token.TOKENS.H:
                                if (g is null) {
                                    throw new ParseException(String.Format("Unexpected {0}", Token.SYMBOLS[(int) Token.TOKENS.H]));
                                }

                                h = new Poly(parseCoefficients(t.argument), mod);
                                break;
                            default:
                                throw new ParseException(String.Format("Unexpected {0}", Token.SYMBOLS[t.token]));
                        }

                        state++;
                        break;
                    case 6:
                        // create operation object from gathered values and start over
                        operations.Add(new Operation(op_code, mod, deg, mod_poly, a, b, f, g, h));

                        // reset values
                        op_code = mod = deg = -1;
                        mod_poly = a = b = f = g = h = null;

                        // we do not want to consume a token in this state, so we decrement i by 1
                        i--;
                        state = 0;

                        // when the token is EOF, we want to step out of the loop
                        if (t.token == (int) Token.TOKENS.EOF) {
                            i = tokens.Count;
                        }

                        break;
                }
            }

            return operations;
        }

        // creates a list of integers from a string in the form "{a,b,c,...,z}" where each element is an integer
        private static List<int> parseCoefficients(string s) {
            if (s == "{}") {
                return new List<int>();
            }

            // remove brackets; split on ','; reverse list; convert every element to integer
            return s.Trim('{', '}').Split(',').Reverse().Select(x => Int32.Parse(x)).ToList();
        }
    }

    class Token {
        public enum TOKENS {MOD, DISPLAY_POLY, ADD_POLY, SUBTRACT_POLY, MULTIPLY_POLY, F, G, H, LONG_DIV_POLY, ANSW_Q, ANSW_R, EUCLID_POLY, ANSW_A, ANSW_B, ANSW_D, EQUALS_POLY_MOD, IRREDUCABLE, DEG, FIND_IRRED, MOD_POLY, ADD_TABLE, MULT_TABLE, DISPLAY_FIELD, ADD_FIELD, SUBTRACT_FIELD, MULTIPLY_FIELD, INVERSE_FIELD, DIVISION_FIELD, A, B, EQUALS_FIELD, PRIMITIVE, FIND_PRIM, ANSWER, EOF};
        public static readonly string[] SYMBOLS = {"[mod]", "[display-poly]", "[add-poly]", "[subtract-poly]", "[multiply-poly]", "[f]", "[g]", "[h]", "[long-div-poly]", "[answ-q]", "[answ-r]", "[euclid-poly]", "[answ-a]", "[answ-b]", "[answ-d]", "[equals-poly-mod]", "[irreducible]", "[deg]", "[find-irred]", "[mod-poly]", "[add-table]", "[mult-table]", "[display-field]", "[add-field]", "[subtract-field]", "[multiply-field]", "[inverse-field]", "[division-field]", "[a]", "[b]", "[equals-field]", "[primitive]", "[find-prim]", "[answer]", "EOF"};

        public int token { get; private set; }
        public string argument { get; private set; }

        public Token(int token, string argument = "") {
            this.token = token;
            this.argument = argument;
        }

        public static bool isOperation(Token t) {
            return (new int[] {1,2,3,4,8,11,15,16,18,20,21,22,23,24,25,26,27,30,31,32}).Contains(t.token);
            // or: return getOperationCode(t) != -1;
        }

        public static bool isInput(Token t) {
            return (new int[] {5,6,7,17,28,29}).Contains(t.token);
        }

        // returns operation code for a given token, and returns -1 if the current token is not an operation
        public static int getOperationCode(Token t) {
            switch(t.token) {
                case (int) TOKENS.DISPLAY_POLY: return (int) Global.OP_CODES.DISPLAY_POLY;
                case (int) TOKENS.ADD_POLY: return (int) Global.OP_CODES.ADD_POLY;
                case (int) TOKENS.SUBTRACT_POLY: return (int) Global.OP_CODES.SUBTRACT_POLY;
                case (int) TOKENS.MULTIPLY_POLY: return (int) Global.OP_CODES.MULTIPLY_POLY;
                case (int) TOKENS.LONG_DIV_POLY: return (int) Global.OP_CODES.LONG_DIV_POLY;
                case (int) TOKENS.EUCLID_POLY: return (int) Global.OP_CODES.EUCLID_POLY;
                case (int) TOKENS.EQUALS_POLY_MOD: return (int) Global.OP_CODES.EQUALS_POLY_MOD;
                case (int) TOKENS.IRREDUCABLE: return (int) Global.OP_CODES.IRREDUCABLE;
                case (int) TOKENS.FIND_IRRED: return (int) Global.OP_CODES.FIND_IRRED;
                case (int) TOKENS.ADD_TABLE: return (int) Global.OP_CODES.ADD_TABLE;
                case (int) TOKENS.MULT_TABLE: return (int) Global.OP_CODES.MULT_TABLE;
                case (int) TOKENS.DISPLAY_FIELD: return (int) Global.OP_CODES.DISPLAY_FIELD;
                case (int) TOKENS.ADD_FIELD: return (int) Global.OP_CODES.ADD_FIELD;
                case (int) TOKENS.SUBTRACT_FIELD: return (int) Global.OP_CODES.SUBTRACT_FIELD;
                case (int) TOKENS.MULTIPLY_FIELD: return (int) Global.OP_CODES.MULTIPLY_FIELD;
                case (int) TOKENS.INVERSE_FIELD: return (int) Global.OP_CODES.INVERSE_FIELD;
                case (int) TOKENS.DIVISION_FIELD: return (int) Global.OP_CODES.DIVISION_FIELD;
                case (int) TOKENS.EQUALS_FIELD: return (int) Global.OP_CODES.EQUALS_FIELD;
                case (int) TOKENS.PRIMITIVE: return (int) Global.OP_CODES.PRIMITIVE;
                case (int) TOKENS.FIND_PRIM: return (int) Global.OP_CODES.FIND_PRIM;
                default: return -1;
            }
        }

        public static int getToken(int op_code) {
            switch(op_code) {
                case (int) Global.OP_CODES.DISPLAY_POLY: return (int) TOKENS.DISPLAY_POLY;
                case (int) Global.OP_CODES.ADD_POLY: return (int) TOKENS.ADD_POLY;
                case (int) Global.OP_CODES.SUBTRACT_POLY: return (int) TOKENS.SUBTRACT_POLY;
                case (int) Global.OP_CODES.MULTIPLY_POLY: return (int) TOKENS.MULTIPLY_POLY;
                case (int) Global.OP_CODES.LONG_DIV_POLY: return (int) TOKENS.LONG_DIV_POLY;
                case (int) Global.OP_CODES.EUCLID_POLY: return (int) TOKENS.EUCLID_POLY;
                case (int) Global.OP_CODES.EQUALS_POLY_MOD: return (int) TOKENS.EQUALS_POLY_MOD;
                case (int) Global.OP_CODES.IRREDUCABLE: return (int) TOKENS.IRREDUCABLE;
                case (int) Global.OP_CODES.FIND_IRRED: return (int) TOKENS.FIND_IRRED;
                case (int) Global.OP_CODES.ADD_TABLE: return (int) TOKENS.ADD_TABLE;
                case (int) Global.OP_CODES.MULT_TABLE: return (int) TOKENS.MULT_TABLE;
                case (int) Global.OP_CODES.DISPLAY_FIELD: return (int) TOKENS.DISPLAY_FIELD;
                case (int) Global.OP_CODES.ADD_FIELD: return (int) TOKENS.ADD_FIELD;
                case (int) Global.OP_CODES.SUBTRACT_FIELD: return (int) TOKENS.SUBTRACT_FIELD;
                case (int) Global.OP_CODES.MULTIPLY_FIELD: return (int) TOKENS.MULTIPLY_FIELD;
                case (int) Global.OP_CODES.INVERSE_FIELD: return (int) TOKENS.INVERSE_FIELD;
                case (int) Global.OP_CODES.DIVISION_FIELD: return (int) TOKENS.DIVISION_FIELD;
                case (int) Global.OP_CODES.EQUALS_FIELD: return (int) TOKENS.EQUALS_FIELD;
                case (int) Global.OP_CODES.PRIMITIVE: return (int) TOKENS.PRIMITIVE;
                case (int) Global.OP_CODES.FIND_PRIM: return (int) TOKENS.FIND_PRIM;
                default: return -1;
            }
        }
    }
}