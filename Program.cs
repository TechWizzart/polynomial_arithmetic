﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace polynomial_arithmetic
{
    class Program
    {
        static void Main(string[] args)
        {
            // Poly field = new Poly(new List<int>() {0, 1}, 7);
            // Poly p = new Poly(new List<int>() {}, 7, field);
            // Console.WriteLine("after p");
            // Poly[,] table = Poly.additionTable(p);

            // Test test = new Test();
            // test.testing();

            if (args.Length < 1) {
                Console.WriteLine("Please specify an input file");
                return;
            }

            string output = "";
            List<Operation> operations = Parser.parse(args[0]);
            operations.ForEach((operation) => output += operation.run() + Environment.NewLine + Environment.NewLine);

            File.WriteAllText("output.txt", output);
        }
    }
}
