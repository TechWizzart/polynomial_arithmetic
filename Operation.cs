using System;
using System.Collections.Generic;
using System.Linq;

namespace polynomial_arithmetic {
    class Operation {
        public readonly int op_code;
        public readonly int mod, deg;
        public readonly Poly mod_poly;
        public readonly Poly a, b, f, g, h;

        public Operation(int op_code, int mod, int deg, Poly mod_poly = null, Poly a = null, Poly b = null, Poly f = null, Poly g = null, Poly h = null) {
            this.op_code = op_code;
            this.mod = mod;
            this.deg = deg;
            this.mod_poly = mod_poly;
            this.a = a;
            this.b = b;
            this.f = f;
            this.g = g;
            this.h = h;
        }

        public string run() {
            List<string> output = new List<string>();

            switch (op_code) {
                // POLYNOMIALS
                case (int) Global.OP_CODES.DISPLAY_POLY:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    // write output(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], f.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.ADD_POLY:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.G], g.toInputString()));
                    // write output(s)
                    Poly result = f + g;
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], result.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.SUBTRACT_POLY:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.G], g.toInputString()));
                    // write output(s)
                    Poly resultSub = f - g;
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], resultSub.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.MULTIPLY_POLY:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.G], g.toInputString()));
                    // write output(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], (f * g).reduce().toString()));
                    break;
                case (int) Global.OP_CODES.LONG_DIV_POLY:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.G], g.toInputString()));
                    // write output(s)
                    Poly resultQ = null, resultR = null;
                    bool divError = false;
                    try {
                        (resultQ, resultR) = Poly.longDivision(f, g);
                    } catch (DivideByZeroException) {
                        divError = true;
                    }

                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSW_Q], divError ? "ERROR" : resultQ.reduce().toString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSW_R], divError ? "ERROR" : resultR.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.EUCLID_POLY:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.G], g.toInputString()));
                    // write output(s)
                    Poly resultA, resultB, resultD;
                    (resultD, resultB, resultA) = Poly.extendedEucledian(f, g);
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSW_A], resultA.reduce().toString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSW_B], resultB.reduce().toString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSW_D], resultD.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.EQUALS_POLY_MOD:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.G], g.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.H], h.toInputString()));
                    // write output(s)
                    bool equals = false;
                    try {
                        equals = Poly.equalModulo(f, g, h);
                    } catch (DivideByZeroException) {
                        equals = false;
                    }
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], equals ? "TRUE" : "FALSE"));
                    break;
                case (int) Global.OP_CODES.IRREDUCABLE:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write input(s)
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.F], f.toInputString()));
                    // write output(s)
                    bool irreducable = f.irreducible();
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], irreducable ? "TRUE" : "FALSE"));
                    break;
                case (int) Global.OP_CODES.FIND_IRRED:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write degree
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.DEG], deg));
                    // find answer
                    Poly irred = Poly.irreducible(mod, deg);
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], irred.toString()));
                    break;
                
                // FINITE FIELDS
                case (int) Global.OP_CODES.ADD_TABLE:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // calculate result
                    Poly[,] additionTable = Poly.additionTable(mod_poly);
                    // construct output string
                    string outTable = "{";

                    for (int i = 0; i < additionTable.GetLength(0); i++) {
                        string tmp = "";
                        for (int j = 0; j < additionTable.GetLength(1); j++) {
                            tmp += additionTable[i, j].toString();
                            if (j+1 != additionTable.GetLength(1)) {
                                tmp += ", ";
                            }
                        }
                        outTable += tmp;

                        if (i + 1 != additionTable.GetLength(0)) {
                            outTable += "; ";
                        } else {
                            outTable += "}";
                        }
                    }

                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], outTable));
                    break;

                case (int) Global.OP_CODES.MULT_TABLE:
                    // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // calculate result
                    Poly[,] multTable = Poly.multiplicationTable(mod_poly);
                    // construct output string
                    string outMultTable = "{";

                    for (int i = 0; i < multTable.GetLength(0); i++) {
                        string tmp = "";
                        for (int j = 0; j < multTable.GetLength(1); j++) {
                            tmp += multTable[i, j].toString();
                            if (j+1 != multTable.GetLength(1)) {
                                tmp += ", ";
                            }
                        }
                        outMultTable += tmp;

                        if (i + 1 != multTable.GetLength(0)) {
                            outMultTable += "; ";
                        } else {
                            outMultTable += "}";
                        }
                    }

                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], outMultTable));
                    break;
                case (int) Global.OP_CODES.DISPLAY_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    // get result and write answer
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], a.makeElement(a.field).reduce().toString()));
                    break;
                case (int) Global.OP_CODES.ADD_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.B], b.toInputString()));
                    // get result and write answer
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], Poly.fieldAddition(a.makeElement(a.field), b.makeElement(b.field)).reduce().toString()));
                    break;
                case (int) Global.OP_CODES.SUBTRACT_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.B], b.toInputString()));
                    // get result and write answer
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], Poly.fieldSubtraction(a.makeElement(a.field), b.makeElement(b.field)).reduce().toString()));
                    break;
                case (int) Global.OP_CODES.MULTIPLY_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.B], b.toInputString()));
                    // get result and write answer
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], Poly.fieldMultiplication(a.makeElement(a.field), b.makeElement(b.field)).reduce().toString()));
                    break;
                case (int) Global.OP_CODES.INVERSE_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    // get result and write answer
                    Poly inverse = null;
                    bool inverseError = false;
                    try {
                        inverse = Poly.inverse(a.makeElement(a.field));
                    } catch (NoInverseException) {
                        inverseError = true;
                    }

                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], inverseError ? "Error" : inverse.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.DIVISION_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.B], b.toInputString()));
                    Poly divResult = null;
                    bool divFieldError = false;
                    try {
                        divResult = (a.makeElement(a.field) / b.makeElement(b.field));
                    } catch (Exception) {
                        divFieldError = true;
                    }
                    // get result and write answer
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], divFieldError ? "ERROR" : divResult.reduce().toString()));
                    break;
                case (int) Global.OP_CODES.EQUALS_FIELD:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.B], b.toInputString()));
                    // get result and write answer
                    bool equalField = a.makeElement(a.field) == b.makeElement(b.field);
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], equalField ? "TRUE" : "FALSE"));
                    break;
                case (int) Global.OP_CODES.PRIMITIVE:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    // write inputs
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.A], a.toInputString()));
                    // get result and write answer
                    bool primitive = a.isPrimitive();
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], primitive ? "TRUE" : "FALSE"));
                    break;
                case (int) Global.OP_CODES.FIND_PRIM:
                     // write mod
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD], mod));
                    // write mod-poly
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.MOD_POLY], mod_poly.toInputString()));
                    // write operation
                    output.Add(String.Format("{0}", Operation.getSymbol(op_code)));
                    Poly prim = null;
                    bool primError = false;
                    try {
                        prim = Poly.findPrimitive(mod_poly);
                    } catch (Exception) {
                        primError = true;
                    }
                    // get result and write answer
                    output.Add(String.Format("{0} {1}", Token.SYMBOLS[(int) Token.TOKENS.ANSWER], primError ? "ERROR" : prim.reduce().toString()));
                    break;

                default: throw new Exception("Illegal operation code");
            }

            // return output as a single string with newlines
            return output.Aggregate("", (acc, s) => acc + Environment.NewLine + s);
        }

        public static string getSymbol(int op_code) {
            return Token.SYMBOLS[Token.getToken(op_code)];
        }
    }
}