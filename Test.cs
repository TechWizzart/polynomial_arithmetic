using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace polynomial_arithmetic
{
    class Test
    {
        private Poly a;
        private Poly b;
        private Poly c;
        private Stopwatch timer;
        public Test () {
            Stopwatch timer = Stopwatch.StartNew();
            this.timer = timer;
            a = new Poly (new List<int>() {1, 1}, 3).makeElement(new Poly(new List<int>() {1, 2, 0, 1}, 3));
            b = new Poly (new List<int>() {2, 1}, 3).makeElement(new Poly(new List<int>() {1, 2, 0, 1}, 3));
            c = new Poly (new List<int>() {}, 7).reduce();
        }

        public Test (int degree, int mod) {
            Random random = new Random();
            List<int> aList = new List<int>();
            List<int> bList = new List<int>();
            for (int i = 0; i < degree; i++) {
                aList.Add(random.Next(1000));
                bList.Add(random.Next(1000));
            }

            this.a = new Poly (aList, mod);
            this.b = new Poly (bList, mod);
        }

        public Test (int degree) {
            Random random = new Random();
            int mod = 11;
            List<int> aList = new List<int>();
            List<int> bList = new List<int>();
            for (int i = 0; i < degree; i++) {
                aList.Add(random.Next(1000));
                bList.Add(random.Next(1000));
            }

            this.a = new Poly (aList, mod);
            this.b = new Poly (bList, mod);
        }

        public void testing () {
            Poly x = Poly.Zero(a.mod);
            Poly y = Poly.Zero(a.mod);
            Poly z = Poly.Zero(a.mod);
            bool result = false;
            try {
                x = Poly.fieldMultiplication(a, b).makeElement(a.field);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine(a.toString() + " * " + b.toString() + " = " + x.toString());
            timer.Stop();
            TimeSpan timespan = timer.Elapsed;
            Console.WriteLine(String.Format("{0} seconds", timespan.Seconds));
        }
    }
}
