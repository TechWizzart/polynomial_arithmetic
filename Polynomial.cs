using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace polynomial_arithmetic {
    class Poly {
        //List the coeffiecients from least significant to most significant: a_0*x^0 -> a_n*x^n for n coefficients, so in the other order than the assignment does
        public readonly ReadOnlyCollection<int> coefficients;
        public readonly int mod;
        public readonly Poly field;

        public Poly (List<int> co, int m, Poly f) {

            if (co.Count == 0) { //If an empty list is provided, put 0 in that list
                co.Add(0);
            }
            //Initialise coefficient values
            this.coefficients = new ReadOnlyCollection<int>(co);

            //Initialise modulus
            this.mod = m;
            //Initialise field polynomial
            this.field = f;

            //Check if the modulus is prime
            bool prime = checkMod(this.mod);

            if (!prime) {
                throw new NotPrimeException(this.toString(), this.mod.ToString());
            }

            if (this.field != null && this.field.field != null) { //If the field polynomial must be an element of another field, throw an exception
                throw new InvalidFieldException();
            }
        }

        public Poly (List<int> co, int m) {
            //Initialise coefficient values
            this.coefficients = new ReadOnlyCollection<int>(co);
            //Initialise modulus
            this.mod = m;

            //Check if the modulus is prime
            bool prime = checkMod(this.mod);
            if (!prime) {
                throw new NotPrimeException(this.toString(), this.mod.ToString());
            }
        }

        // gives a representation of the polynomial in the the input-format
        public string toInputString() {
            string s = "{";

            for (int i = coefficients.Count - 1; i >= 0; i--) {
                s += coefficients[i];
                if (i == 0) {
                    s += "}";
                } else {
                    s += ",";
                }
            }

            return s;
        }

        public string toString() {
            string tmp = "";
            bool nonzero = false;
            
            //Read the list backwards and construct the string with coefficients > 1 and with the X's if the coefficient is nonzero
            for (int i = coefficients.Count - 1; i >= 0; i--) {
                if (coefficients[i] != 0) {
                    nonzero = true;
                    //If not the first iteration, add a +
                    if (i != coefficients.Count - 1) {
                        tmp += " + ";
                    }

                    if (i > 1 && coefficients[i] != 1) {
                        tmp += coefficients[i].ToString() + "X^" + i;
                    } else if (i > 1) {
                        tmp += "X^" + i;
                    } else if (i == 1 && coefficients[i] != 1) {
                        tmp += coefficients[i].ToString() + "X";
                    } else if (i == 1) {
                        tmp += "X";
                    } else {
                        tmp += coefficients[i].ToString();
                    }
                }
            }

            // if (!(field is null)) {
            //     tmp += " + (" + field.toString() + ")";
            // }

            if (!nonzero) {
                tmp = "0";
            }

            //Output the constructed string
            return tmp;
        }

        //Construct the Zero polynomial if needed
        public static Poly Zero (int mod, Poly field = null) {
            return new Poly(new List<int>() {0}, mod, field);
        }

        public static Poly One(int mod, Poly field = null) {
            return new Poly(new List<int>() {1}, mod, field);
        }

        public static Poly operator +(Poly a, Poly b) {
            //Implement addition algorithm

            // Check which coefficient is bigger, then take that value as the total elements for the result.
            int result_index = Math.Max(a.coefficients.Count, b.coefficients.Count);
            // Create a new list for the result.
            List<int> result = new List<int>();
                 
            // Loop through the list of the result up till the last index which is result_index.
            for (int i = 0; i < result_index; i++) {  
                // if the index of the result is greater than the index of a then add the remaining coefficients of b to the result list.             
                 if (i >= a.coefficients.Count) {
                     result.Add(b.coefficients[i]);
                     // if the index of the result is greater than the index of b then add the remaining coefficients of a to the result list.
                 } else if (i >= b.coefficients.Count) {
                     result.Add(a.coefficients[i]);
                     // else when they are of equal length then add each coefficient of a and b. 
                 } else {
                     result.Add(a.coefficients[i] + b.coefficients[i]);
                 }
            }
            // return the result as a polynomial mod a prime number.            
            return new Poly(result, b.mod).reduce();
        }

        public static Poly operator -(Poly a, Poly b) {
            //Implement subtraction algorithm
           
            // Check which coefficient is bigger, then take that value as the total elements for the result.
            int result_index = Math.Max(a.coefficients.Count, b.coefficients.Count);
            // Create a new list for the result.
            List<int> result = new List<int>();
                 
            // Loop through the list of the result up till the last index which is result_index.
            for (int i = 0; i < result_index; i++) {  
                // if the index of the result is greater than the index of a then add the remaining coefficients of 0 - b made to the result list.             
                 if (i >= a.coefficients.Count) {
                     result.Add(-b.coefficients[i]);
                     // if the index of the result is greater than the index of b then add the remaining coefficients of a to the result list.
                 } else if (i >= b.coefficients.Count) {
                     result.Add(a.coefficients[i]);
                     // else when they are of equal length then subtract each coefficient of a minus b. 
                 } else {
                     result.Add(a.coefficients[i] - b.coefficients[i]);
                 }
            }
            // return the result as a polynomial mod a prime number.            
            return new Poly(result, b.mod).reduce();
        }
        //Multiplication algorithm assumes a and b have the same modulus
        public static Poly operator *(Poly x, Poly y) {
            int[] array = new int[x.coefficients.Count + y.coefficients.Count];
            for (int i = 0; i < x.coefficients.Count; i++) {
                for (int j = 0; j < y.coefficients.Count; j++) {
                    array[i + j] += x.coefficients[i] * y.coefficients[j];
                }
            }

            //Turn array into a list
            Poly z = new Poly(new List<int>(array), x.mod);
            z = z.reduce(); //Reduce the coefficients
            z = z.removeZeroes(); //Remove leading zeroes
            return z;
        }

        //Reduce polynomial a modulo polynomial b
        public static Poly operator %(Poly a, Poly b) {
            Poly r = longDivision(a, b).Item2;
            return r;
        }

        public static Tuple<Poly, Poly> longDivision(Poly a, Poly b) {
            //assumes b is not equal to zero
            Poly q = Zero(a.mod); //quotient is set to zero
            Poly r = a.reduce(); //remainder is set equal to a
            if (b == Poly.Zero(b.mod)) {
                throw new DivideByZeroException();
            }

            //while degree of r >= degree of b we are in this while loop
            while (r.degree() >= b.degree()) {
                //example: a * x^n
                //lc is the leading coefficient of the polynomial, from example this is 'a'
                //degree is the power, from example this is 'n'
                int lc_r = r.coefficients.Last();
                int lc_b = b.coefficients.Last();

                int deg = r.degree() - b.degree();
                int result = lc_r / lc_b;

                //If the result is 0 and lc_r is not 0, then find the inverse of lc_b and use that as the result of lc_r/lc_b
                if (lc_r != 0 && result == 0) {
                    (int z, int x, int y) = ee(lc_b, b.mod);
                    result = x;
                }
                List<int> coefficients = new List<int>(new int[deg + 1]);
                coefficients[deg] = result;
                Poly p = new Poly(coefficients, a.mod);

                q = q + p;
                r = r - (p * b);
            }        
           
            q = q.reduce();
            r = r.reduce();

            return Tuple.Create(q, r);
        }

        public static Tuple<Poly, Poly, Poly> extendedEucledian (Poly a, Poly b) {
            // q will be overwritten in the loop, initialization is just to remove compiler errors
            Poly q = Poly.Zero(a.mod);
            Poly r;
            Poly x = One(a.mod);
            Poly v = One(a.mod);
            Poly y = Zero(a.mod);
            Poly u = Zero(a.mod);
            Poly x_prime;
            Poly y_prime;
            //TODO Implement extended eucledian algorithm
            while (b != Zero(a.mod)) {
                (q, r) = longDivision(a, b);
                a = b;
                b = r;
                x_prime = x;
                y_prime = y;
                x = u;
                y = v;
                u = x_prime - q * u;
                v = y_prime - q * v;
            }
            int lc_a = a.coefficients.Last();

            if (!(a.field is null)) {
                a = longDivision(a, new Poly(new List<int>() {lc_a}, a.mod)).Item1.makeElement(a.field); 
                x = longDivision(x, new Poly(new List<int>() {lc_a}, x.mod)).Item1.makeElement(a.field); 
                y = longDivision(y, new Poly(new List<int>() {lc_a}, y.mod)).Item1.makeElement(a.field); 
            }
            // divide q, x and y by lc_a
            a = longDivision(a, new Poly(new List<int>() {lc_a}, a.mod)).Item1.reduce(); 
            x = longDivision(x, new Poly(new List<int>() {lc_a}, x.mod)).Item1.reduce(); 
            y = longDivision(y, new Poly(new List<int>() {lc_a}, y.mod)).Item1.reduce(); 

            //not done yet
            //return x*leading coefficient(a)^-1, y*leading coefficient(a)^-1
            return Tuple.Create(a, x, y);
        }

        public static bool equalModulo (Poly a, Poly b, Poly c) {
            return a % c == b % c;
        }

        public static Poly fieldSubtraction (Poly x, Poly y) {
            try {
                validate(x, y); //Check if a and b are elements of the same field
            } catch (Exception e) {
                Console.WriteLine(e.Message); //If an exception occurs output the exception
                return null; //return null
            }

            int max = Math.Max(x.coefficients.Count, y.coefficients.Count);
            List<int> outList = new List<int>();

            //For each coefficient from x^0 to the largest power of x
            for (int i = 0; i < max; i++) {
                int z;
                if (i >= x.coefficients.Count) { //x has no x^i (and higher powers of x) so the subtraction becomes 0 - y for coefficient x^i
                    z = -y.coefficients[i];
                } else if (i >= y.coefficients.Count) { //y has no x^i (and higher powers of x) so the subtraction becomes x - 0 for coefficient x^i
                    z = x.coefficients[i];
                } else {
                    z = x.coefficients[i] - y.coefficients[i]; //Do x - y for coefficient x^i
                }

                if(z < 0) { //If result is less than 0
                    z += x.mod; //Make z positive
                }
                outList.Add(z); //Add digit to the list
            }
            Poly result = new Poly(outList, x.mod);
            result = result.makeElement(x.field); //Do all necessary operations to make result an element of the field
            return result;
        }

         public static Poly fieldAddition (Poly a, Poly b) {
             // Addition Operation on elements of a Field
            try {
                validate(a, b); //Check if a and b are elements of the same field
            } catch (Exception e) {
                Console.WriteLine(e.Message); //If an exception occurs output the exception
                return null; //return null
            }

            // check what the max size of the new list for the result is
            int result_index = Math.Max(a.coefficients.Count, b.coefficients.Count);
            // create a list for the result
            List<int> result = new List<int>(result_index);

            //For each coefficient from a^0 to the largest power of a
            for (int i = 0; i < result_index; i++) {
                // if the index is larger than the last index of a than add the remaining coefficients of b to the result list.
               if (i >= a.coefficients.Count) {
                   result.Add(b.coefficients[i]);
                   // if the index is larger than the last index of b than add the remaining coefficients of a to the result list.
               } else if (i >= b.coefficients.Count) {
                   result.Add(a.coefficients[i]);
                   // else add each coefficients of a and b together
               } else {
                   result.Add(a.coefficients[i] + b.coefficients[i]);  //Do a + b for coefficient a^i
               }
            }

            // create a new polynomial with the result list and a given mod.
            Poly result2 = new Poly(result, a.mod);
            // tranform result2 such that it is an element of the field.
            result2 = result2.makeElement(a.field); //Do all necessary operations to make result2 an element of the field
            // return output
            return result2;
        }

        public static Poly fieldMultiplication (Poly a, Poly b) {
            try {
                validate(a, b); //Check if a and b are elements of the same field
            } catch (Exception e) {
                Console.WriteLine(e.Message); //If an exception occurs output the exception
                return null; //return null
            }

            int[] array = new int[a.coefficients.Count + b.coefficients.Count];
            //turn the array into a list
            for (int i = 0; i < a.coefficients.Count; i++) {
                for (int j = 0; j < b.coefficients.Count; j++) {
                    array[i + j] += a.coefficients[i] * b.coefficients[j];
                }
            }
            
            //create a new polynomial with the list previously created and the mod that was given
            Poly d = new Poly(new List<int>(array), a.mod);
            //make the polynomial and element of the field
            d = d.makeElement(a.field);
            return d;
        }



        public static Poly inverse (Poly a) {
            try {
                validate(a); //Check if a is an element of a field
            } catch (Exception e) {
                Console.WriteLine(e.Message); //If not output an error message and return null
                return null;
            }

            Poly q; 
            Poly x;
            Poly y;
            (q, x, y) = extendedEucledian(a, a.field);

            if (q == Poly.One(q.mod) && a.field.irreducible()) {
                return x; //Output the multiplicative inverse if gcd == 1
            } else {
                throw new NoInverseException(a.toString()); //Else throw a NoInverseException
            }
        }

        //Division operator ONLY for finite field elements, use longDivision for normal polynomials!
        //Can throw NoFieldExceptions, FieldExceptions and InverseExceptions
        public static Poly operator / (Poly x, Poly y){
            validate(x, y);

            Poly i;
            i = inverse(y); //Calculate inverse of y

            Poly r = x * i;
            r = r.makeElement(x.field); //Do all necessary operations to make r an element of the field
            return r;
        }

        // returns this^n (polynomial raised to the power n)
        // works only for n >= 0
        public Poly pow(int n) {
            if (n == 0) {
                return Poly.One(this.mod, this.field);
            }

            Poly result = this;

            while (--n > 0) {
                result *= this;
                result = result.makeElement(this.field);
            }

            return result;
        }

        /*
         * Remove leading zeroes from the instance that called it. Can only be used within the Polynomial class
         */
        private Poly removeZeroes () {
            List<int> coefficients = new List<int>(this.coefficients);

            int k = coefficients.Count - 1; //Count amount of coefficients
            if (k <= 0) { //If the count is negative or zero, do nothing and return the poly
                return new Poly(coefficients, this.mod);
            }

            while (coefficients[k] == 0 && k > 0) { //While the leading coefficients are 0 and k > 0
                coefficients.RemoveAt(k); //Remove the leading zero
                k--; //Decrease the coefficient count by 1
            }

            if (this.field is null) {
                return new Poly(coefficients, this.mod);
            } else {
                return new Poly(coefficients, this.mod, this.field);
            }
        }

        //Reduce all coefficients to be within the interval [0, ... , m-1] for modulus m
        public Poly reduce() {
            List<int> outList = new List<int>();
            for (int i = 0; i < this.coefficients.Count; i++) {
                int reduced = this.coefficients[i] % this.mod;
                if (reduced < 0) {
                    reduced += this.mod;
                }
                outList.Add(reduced);
            }

            if (this.field is null) {
                return new Poly(outList, this.mod).removeZeroes();
            } else {
                return new Poly(outList, this.mod, this.field).removeZeroes();
            }
        }

        public bool irreducible() {
            int t = 0;

            Poly irreducable;
            do {
                t++;
                // construct X^(q^t) - X
                int qt = (int) Math.Pow(this.mod, t);

                List<int> coefficients = new List<int>();
                for (int i = 0; i <= qt; i++) coefficients.Add(0);

                coefficients[1] = -1;
                coefficients[qt] = 1;
                irreducable = new Poly(coefficients, this.mod);
            } while (extendedEucledian(this, irreducable).Item1 == Poly.One(this.mod));

            return t == this.degree();
        }

        // returns an irreducible polynomial of degree n
        public static Poly irreducible(int mod, int degree) {
            Random rnd = new Random();
            Poly f;

            do {
                List<int> coefficients = new List<int>();
                for (int i = 0; i < degree; i++) {
                    coefficients.Add(rnd.Next(0, mod));
                }

                // make sure the last element is not zero, so f has the correct degree
                coefficients.Add(rnd.Next(1, mod));
                // construct poly object
                f = new Poly(coefficients, mod);
            } while (! f.irreducible());

            return f;
        }

        public static Poly[,] additionTable(Poly poly) {
            //Creating Addition Table for Field = Z/pZ[X]/q(X), irreducible polynomial q, prime p
            // p is given prime number (a mod)
            int p = poly.mod;
            // the irreducible polynomial q
            Poly q = poly;
            // a list created for all the coefficients up till the given prime number (mod number) that can be used to define the equivalence classes
            List<int> maxCoefficients = new List<int>();
            
            // add all the numbers up till the prime number to the list
            for (int i = 0; i < p; i++) {
                maxCoefficients.Add(i);
            }
        
            // the amount of elements in a field is equal to p^(degree of q).
            int elementsOfField = (int) Math.Pow((double) p, (double) q.degree());
            // a list with the elements of the field in it
            List<Poly> elements = fieldElements(maxCoefficients, q.degree() - 1).Select(coefficients => new Poly(coefficients, p)).ToList();

            // creating the table with the size: #elements in the field x #elements in the field
            Poly[,] table = new Poly[elementsOfField, elementsOfField];
            //for each entry calculate the addition.
            for (int i = 0; i < elementsOfField; i++) {
                for (int j = 0; j < elementsOfField; j++) {
                    table[i,j] = elements[i] + elements[j];
                }
            }
            // return table
            return table;
        }

        public static Poly[,] multiplicationTable(Poly poly) {
            /**
            Create the multiplication table for Field = Z/pZ[X]/q(X)
            with q being the irreducible polynomial and
            p the given prime number (a mod)
             */
            int p = poly.mod;
            Poly q = poly;

            List<int> totalOfCoefficients = new List<int>();

            for (int i = 0; i < p; i++) {
                totalOfCoefficients.Add(i);
            }
            
            //amount of elements in field is equal to p^(degree(q))
            int elementsOfField = (int)Math.Pow((double)p, (double)q.degree());
            //list with all the elements of the field
            List<Poly> elements = fieldElements(totalOfCoefficients, q.degree() - 1).Select(coefficients => new Poly(coefficients, p)).ToList();

            //table with the size: #elements in the field x #elements in the field
            Poly[,] table = new Poly[elementsOfField, elementsOfField];
            //for each entry calculate the multiplication that has to be executed
            for (int i = 0; i < elementsOfField; i++) {
                for (int j = 0; j < elementsOfField; j++) {
                    table[i,j] = elements[i] * elements[j];
                }
            }

            //returns the table
            return table;
        }


        // Returns a list of the coefficients of all the elements in a field (defined by the possible coefficients and the max-degree)
        public static List<List<int>> fieldElements(List<int> possibleCoefficients, int maxDegree) {
            // if the degree of the possible equivalence classes is at most 0 then return the constants only
            if (maxDegree <= 0) {
                return possibleCoefficients.Select(x => new List<int>() {x}).ToList();
            }
            
            //create the output as a list with as elements; several lists.
            List<List<int>> output = new List<List<int>>();

            List<List<int>> res = fieldElements(possibleCoefficients, maxDegree - 1);

            for (int i = 0; i < possibleCoefficients.Count; i++) {
                List<List<int>> tmp = res.Select(x => new List<int>(x.Select(y => y))).ToList();
                tmp.ForEach(x => x.Add(possibleCoefficients[i]));
                output.AddRange(tmp);
            }

            return output;
        }

        public bool isPrimitive(List<int> primeFactors = null) {
            // validate this is an element of a field
            Poly p = this;
            Poly.validate(p);

            // make p proper element of p.field
            p = p.makeElement(p.field);

            // declare ints
            int q, i, k;

            // set q equal to the amount of elements in the field
            q = (int) Math.Pow(p.field.mod, p.field.degree());

            // check if the primefactors are given and if not, generate them
            if (primeFactors == null) {
                primeFactors = Poly.primeDivisors(q-1);
            }

            // initialize i
            i = 0;
            // initialize k
            k = primeFactors.Count - 1;

            // increase i until the result of p^(q-1/primefactor) is not the One polynomial
            while (i <= k && p.pow((q-1) / primeFactors[i]) != Poly.One(p.mod, p.field)) {
                i++;
            }

            // element is not primitive if i is not greater than k
            if (i <= k) {
                return false;
            }

            // else it is
            return true;
        }

        public static Poly findPrimitive(Poly field) {
            // validate field
            if (!field.irreducible()) {
                throw new ReducibleException(field.toString());
            }

            // first gather the prime factors for q-1
            int q = (int) Math.Pow(field.mod, field.degree());
            List<int> primeFactors = Poly.primeDivisors(q-1);

            // construct a random element in F
            Random rnd = new Random();

            List<int> randomCoefficients = new List<int>(new int[q]);
            Poly primitive;

            do {
                for (int i = 0; i < q; i++) {
                    randomCoefficients[i] = rnd.Next(0, field.mod);
                }

                primitive = new Poly(randomCoefficients, field.mod).makeElement(field);
            } while (!primitive.isPrimitive(primeFactors));

            return primitive.makeElement(field);
        }

        /*
         * Check if integer m is a prime number, by checking every integer that could divide m
         */
        public static bool checkMod (int m) {
            int l = (int) Math.Floor(Math.Sqrt((double) m));

            for (int i = 2; i <= l; i++) {
                if (m % i == 0) return false;
            }

            return true;

            // if (m < 3 && m > 0) { //m == 1 or m == 2, then prime by definition
            //     return true;
            // } else if (m % 2 == 0) { //If m is even, m is not a prime by definition
            //     return false;
            // }

            // int R = 5; //Run the check for 5 different a's
            // int r = 0; //Check counter
            // bool prime = true; //Boolean that remembers whether m is probably prime
            // int n = m - 1;
            // int s = 0;
            // int t = 0;
            // //Compute s and t such that m-1 == 2^s * t
            // while (n != Math.Pow(2, s) * t) {
            //     s++;
            //     t = (int)(n / Math.Pow(2, s)); //Assign t to be n/2^s
            //     if (t % 2 == 0) {
            //         t = 1;
            //     }
            // }
            // Random random = new Random();

            // while (prime && r < R) {
            //     int a = random.Next(n); //Pick random a less than n-1
            //     while (a < 2) { //If a < 2 pick a new random a untill a >= 2
            //         a = random.Next(n);
            //     }
            //     int k = 0;
            //     int x = (int)Math.Pow(a, t) % m;
            //     int z = 0;
            //     while (k < s && x % m != 1) {
            //         k++;
            //         z = x;
            //         x = (int)Math.Pow(x, 2) % m;
            //     }

            //     if (k == 0) {
            //         r++;
            //     } else if (k == s && x % m != 1) {
            //         prime = false;
            //     } else if (z % m == m-1) {
            //         r++;
            //     } else {
            //         prime = false;
            //     }
            // }

            // return prime;
        }

        public Poly makeElement(Poly field) {
            Poly r;
            r = this.reduce(); //Reduce coeffiecients
            r = r % field; //Make r a proper element of the field
            r = r.removeZeroes(); //Remove unnecessary zeroes
            return new Poly(r.coefficients.ToList(), r.mod, field);
        }

        private static void validate (Poly a, Poly b) {
            if (a.field is null) { //Check if a is an element of a field
                throw new NoFieldException(a.toString());                
            } else if (b.field is null) { //Check if b is an element of a field
                throw new NoFieldException(b.toString());
            } else if (a.field != b.field) { //Check if a and b are elements of the same field
                throw new FieldException(a.toString(), b.toString());
            }
        }

        private static void validate (Poly a) {
            if (a.field is null) { //Check if a is an element of a field
                throw new NoFieldException(a.toString());
            }
        }

        public static bool operator == (Poly x, Poly y) {
            // if both are null, return true
            if (Object.ReferenceEquals(x, null) && Object.ReferenceEquals(y, null)) {
                return true;
            }
            
            // if either one is null, return false
            if (Object.ReferenceEquals(x, null) ^ Object.ReferenceEquals(y, null)) {
                return false;
            }

            if (x.coefficients.Count != y.coefficients.Count) {
                return false;
            }

            //Check for each coefficient if they are equal, if not return false
            for (int i = 0; i < x.coefficients.Count; i++) {
                if (x.coefficients[i] != y.coefficients[i]) {
                    return false;
                }
            }

            // Check if mod is the same
            if (x.mod != y.mod) {
                return false;
            }

            return x.field == y.field;
        }

        public static bool operator != (Poly a, Poly b) {
            return !(a == b);
        }

        public override bool Equals(object obj) {
            if (obj == null || !this.GetType().Equals(obj.GetType())) {
                return false;
            }

            Poly p = (Poly) obj;
            return this == p;
        }

        // to shut up warnings we might as well define equality properly
        public override int GetHashCode() {
            unchecked{
                int coefficientsHashCode = !(this.coefficients?.Any() == false) ? this.coefficients.GetHashCode() : 0;
                int fieldHashCode = this.field != null ? this.field.GetHashCode() : 0;

                int hashCode = 13;
                hashCode = (hashCode * 397) ^ this.mod;
                hashCode = (hashCode * 397) ^ coefficientsHashCode;
                hashCode = (hashCode * 397) ^ fieldHashCode;
                return hashCode;
            }
        }

        public int degree() {
            Poly x = this.reduce().removeZeroes();
            //Check if x is equal to 0 (Poly.Zero appearantly doesn't work :( )
            if (x.coefficients.Count == 1 && x.coefficients[0] == 0) {
                return -1;
            }
            return x.coefficients.Count - 1;
        }

        // Helper function that gives the prime factors of an integer
        // not related to Polynomials
        public static List<int> primeDivisors(int n ) {
            List<int> factors = new List<int>();

            for (int i = 2; n > 1; i++) {
                if (n % i == 0) {
                    factors.Add(i);
                    while (n % i == 0) {
                        n = n / i;
                    }
                }
            }

            return factors;
        }

        private static Tuple<int, int, int> ee (int a, int b) {
            int ap = Math.Abs(a);
            int bp = Math.Abs(b);
            int x1 = 1;
            int x2 = 0;
            int y1 = 0;
            int y2 = 1;
            int q;
            int r;
            while (bp > 0) {
                q = ap / bp;
                r = ap - (q * bp);
                ap = bp;
                bp = r;
                int x3 = x1 - (q * x2);
                int y3 = y1 - (q * y2);
                x1 = x2;
                y1 = y2;
                x2 = x3;
                y2 = y3;
            }
            int d = ap;
            int x;
            int y;
            if (a >= 0) {
                x = x1;
            } else {
                x = -x1;
            }

            if (b >= 0) {
                y = y1;
            } else {
                y = -y1;
            }
            return Tuple.Create(d, x, y);
        }
    }
}